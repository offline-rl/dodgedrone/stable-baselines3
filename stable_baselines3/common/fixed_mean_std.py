from dataclasses import dataclass

import numpy as np


@dataclass
class FixedMeanStd:
    mean: np.ndarray
    std: np.ndarray
    epsilon: float
