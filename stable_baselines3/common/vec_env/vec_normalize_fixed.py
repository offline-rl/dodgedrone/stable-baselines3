from __future__ import annotations

import pickle
from copy import deepcopy
from typing import Any, Dict, Union, cast

import gym
import numpy as np

from stable_baselines3.common import utils
from stable_baselines3.common.fixed_mean_std import FixedMeanStd
from stable_baselines3.common.vec_env.base_vec_env import VecEnv, VecEnvStepReturn, VecEnvWrapper


class VecNormalizeFixed(VecEnvWrapper):
    """
    A moving average, normalizing wrapper for vectorized environment.
    has support for saving/loading moving average,

    :param venv: the vectorized environment to wrap
    :param epsilon: To avoid division by zero
    """

    def __init__(
        self,
        venv: VecEnv,
        mean: np.ndarray,
        std: np.ndarray,
        epsilon: float = 1e-3,
    ):
        VecEnvWrapper.__init__(self, venv)

        self.obs_spaces = None
        self.obs_rms = FixedMeanStd(mean=mean, std=std, epsilon=epsilon)
        self.old_obs = np.array([])

        self._sanity_checks()

    def _sanity_checks(self) -> None:
        if not isinstance(self.observation_space, gym.spaces.Box):
            raise NotImplementedError(
                f"VecNormalize only supports `gym.spaces.Box` environments, not {self.observation_space}."
            )

        assert self.obs_rms.mean.shape == self.observation_space.shape
        assert self.obs_rms.std.shape == self.observation_space.shape

    def __getstate__(self) -> Dict[str, Any]:
        state = self.__dict__.copy()
        # these attributes are not pickleable
        del state["venv"]
        del state["class_attributes"]
        return state

    def __setstate__(self, state: Dict[str, Any]) -> None:
        """
        Restores pickled state.

        User must call set_venv() after unpickling before using.

        :param state:"""
        # Backward compatibility
        self.__dict__.update(state)
        assert "venv" not in state
        self.venv = None

    def set_venv(self, venv: VecEnv) -> None:
        """
        Sets the vector environment to wrap to venv.

        Also sets attributes derived from this such as `num_env`.

        :param venv:
        """
        if self.venv is not None:
            raise ValueError(
                "Trying to set venv of already initialized VecNormalize wrapper."
            )
        VecEnvWrapper.__init__(self, venv)

        # Check only that the observation_space match
        utils.check_for_correct_spaces(venv, self.observation_space, venv.action_space)

    def step_wait(self) -> VecEnvStepReturn:
        """
        Apply sequence of actions to sequence of environments
        actions -> (observations, rewards, dones)

        where ``dones`` is a boolean vector indicating whether each element is new.
        """
        obs, rewards, dones, infos = self.venv.step_wait()
        self.old_obs = obs

        obs = self.normalize_obs(obs)

        # Normalize the terminal observations
        for idx, done in enumerate(dones):
            if not done:
                continue
            if "terminal_observation" in infos[idx]:
                infos[idx]["terminal_observation"] = self.normalize_obs(
                    infos[idx]["terminal_observation"]
                )

        return obs, rewards, dones, infos

    def _normalize_obs(self, obs: np.ndarray, obs_rms: FixedMeanStd) -> np.ndarray:
        """
        Helper to normalize observation.
        :param obs:
        :param obs_rms: associated statistics
        :return: normalized observation
        """
        return (obs - obs_rms.mean) / (obs_rms.std + obs_rms.epsilon)

    def _unnormalize_obs(self, obs: np.ndarray, obs_rms: FixedMeanStd) -> np.ndarray:
        """
        Helper to unnormalize observation.
        :param obs:
        :param obs_rms: associated statistics
        :return: unnormalized observation
        """
        return (obs * (obs_rms.std + obs_rms.epsilon)) + obs_rms.mean

    def normalize_obs(
        self, obs: Union[np.ndarray, Dict[str, np.ndarray]]
    ) -> Union[np.ndarray, Dict[str, np.ndarray]]:
        """
        Normalize observations using this VecNormalize's observations statistics.
        Calling this method does not update statistics.
        """
        # Avoid modifying by reference the original object
        obs_ = deepcopy(obs)
        obs_ = self._normalize_obs(obs, self.obs_rms).astype(np.float32)
        return obs_

    def unnormalize_obs(
        self, obs: Union[np.ndarray, Dict[str, np.ndarray]]
    ) -> Union[np.ndarray, Dict[str, np.ndarray]]:
        # Avoid modifying by reference the original object
        obs_ = deepcopy(obs)
        obs_ = self._unnormalize_obs(obs, self.obs_rms)
        return obs_

    def get_original_obs(self) -> Union[np.ndarray, Dict[str, np.ndarray]]:
        """
        Returns an unnormalized version of the observations from the most recent
        step or reset.
        """
        return deepcopy(self.old_obs)

    def reset(self) -> Union[np.ndarray, Dict[str, np.ndarray]]:
        """
        Reset all environments
        :return: first observation of the episode
        """
        obs = self.venv.reset()
        self.old_obs = obs
        return self.normalize_obs(obs)

    @staticmethod
    def load(load_path: str, venv: VecEnv) -> VecNormalizeFixed:
        """
        Loads a saved VecNormalize object.

        :param load_path: the path to load from.
        :param venv: the VecEnv to wrap.
        :return:
        """
        with open(load_path, "rb") as file_handler:
            vec_normalize = pickle.load(file_handler)
        vec_normalize.set_venv(venv)
        return cast(VecNormalizeFixed, vec_normalize)

    def save(self, save_path: str) -> None:
        """
        Save current VecNormalize object with
        all running statistics and settings (e.g. clip_obs)

        :param save_path: The path to save to
        """
        with open(save_path, "wb") as file_handler:
            pickle.dump(self, file_handler)
